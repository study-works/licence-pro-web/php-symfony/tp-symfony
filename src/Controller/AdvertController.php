<?php

namespace App\Controller;

use App\Entity\Advert;
use App\Form\AdvertType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdvertController extends AbstractController
{
    /**
     * @Route("/annonces.html", name="app.advert_all")
     */
    public function showAllAction()
    {
        $advertArray = $this->getDoctrine()
            ->getRepository(Advert::class)
            ->findAllJoinToCategory();

        return $this->render('advert/advert.html.twig', [
            'advertArray' => $advertArray,
        ]);
    }

    /**
     * @Route("/nouvelle-annonce.html", name="app.advert_create", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        return $this->processsForm($request, new Advert());
    }

    /**
     * @param Request $request
     * @param Advert $advert
     *
     * @return RedirectResponse|Response
     */
    private function processsForm(Request $request, Advert $advert)
    {
        $form = $this->createForm(AdvertType::class, $advert);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $advert = $form->getData();
            $advert->setCreationDate(new \DateTime());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($advert);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('app.advert_all'));
        }

        return $this->render('advert/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
