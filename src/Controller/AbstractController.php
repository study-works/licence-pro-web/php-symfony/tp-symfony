<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as SymfonyAbstractController;
use Symfony\Contracts\Cache\CacheInterface;

class AbstractController extends SymfonyAbstractController
{
    /** @var CacheInterface */
    protected $cache;

    /**
     * @param CacheInterface $cache configured in cache.yml (pools.main.cache)
     */
    public function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }
}
