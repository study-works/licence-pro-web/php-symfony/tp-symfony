<?php

namespace App\DataFixtures;

use App\Entity\Advert;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixture extends BaseFixture
{
    public const ADVERT_TITLE_ARRAY = [
        "Divers",
        "Livre",
        "Figurine",
        "Téléphone",
        "Voiture",
    ];

    protected function loadData(ObjectManager $manager)
    {
        foreach (self::ADVERT_TITLE_ARRAY as $advertTitle) {
            $category = new Category();
            $category->setName($advertTitle)
                ->setColor($this->faker->hexColor)
            ;
            $manager->persist($category);

            $this->addReference($advertTitle, $category);
        }

        $manager->flush();
    }
}
